# RGB Control
- Arduino UNO R3 * 1
- TIP120 * 3
- LED strip 5050


### Pin
| Name | Pin |
| ------ | ------ |
| RED | 3 |
| GREEN | 5 |
| BLUE | 6 |
| + | 5V |