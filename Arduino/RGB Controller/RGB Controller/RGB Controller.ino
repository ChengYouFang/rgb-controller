﻿/*
 Name:		RGB_Controller.ino
 Created:	2018/5/2 上午 12:08:26
 Author:	ChengYou
*/

//Red
#define RED 3
//Green
#define GREEN 5
//Blue
#define BLUE 6



// the setup function runs once when you press reset or power the board
void setup() {
	Serial.begin(9600);
	pinMode(RED, OUTPUT);
	pinMode(GREEN, OUTPUT);
	pinMode(BLUE, OUTPUT);
}

// the loop function runs over and over again until power down or reset
void loop() {
	int r = random(0, 255);
	int g = random(0, 255);
	int b = random(0, 255);
	analogWrite(RED, r);
	analogWrite(GREEN, g);
	analogWrite(BLUE, b);
	delay(1000);
}

